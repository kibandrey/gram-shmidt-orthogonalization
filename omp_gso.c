#include <iostream>
#include <cstdlib>
#include <cmath>
#include "omp.h"

/* Margin of error. */
const double eps = 0.1;

/* Gram-Schmidt process. */
void gramSchmidt(double *A, int dim)
{
    for (int i = 0; i < dim; ++i)
    {
        /* Calculate vector norm. */
        double tmp = 0.;
        #pragma omp parallel for reduction(+:tmp)
        for (int j = 0; j < dim; ++j)
        {
            tmp += A[i * dim + j] * A[i * dim + j];
        }
        tmp = std::sqrt(tmp);

        /* Normalize vector. */
        #pragma omp parallel for
        for (int j = 0; j < dim; ++j)
        {
            A[i * dim + j] /= tmp;
        }

        /* Subtract projections. */
        #pragma omp parallel for reduction(+:tmp) schedule(dynamic)
        for (int j = i + 1; j < dim; ++j)
        {
            tmp = 0.;
            for (int k = 0; k < dim; ++k)
            {
                tmp += A[i * dim + k] * A[j * dim + k];
            }
            for (int k = 0; k < dim; ++k)
            {
                A[j * dim + k] -= tmp * A[i * dim + k];
            }
        }
    }
}

/* Check results. */
void check(const double *A, int dim)
{
    for (int i = 0; i < dim; ++i)
    {
        for (int j = 0; j < dim; ++j)
        {
            double sum = 0.;
            for (int k = 0; k < dim; ++k)
            {
                sum += A[i * dim + k] * A[j * dim + k];
            }
            if (i == j)
            {
                if (std::abs(sum - 1.) > eps)
                {
                    std::cerr << "Failed unit length: " << i << ", " << j << ", " << sum << std::endl;
                    exit(1);
                }
            }
            else
            {
                if (std::abs(sum) > eps)
                {
                    std::cerr << "Failed orthogonality: " << i << ", " << j << ", " << sum << std::endl;
                    exit(1);
                }
            }
        }
    }
}

int main(int argc, char *argv[])
{
    srand(time(NULL));
    int dim = atoi(argv[1]);
    double *A = new double[dim * dim];

    /* Generate set of random vectors. */
    for (int i = 0; i < dim * dim; ++i)
    {
        A[i] = rand() / (RAND_MAX + 1.);
    }

    /* Measure performance. */
    double start, stop;
    start = omp_get_wtime();
    gramSchmidt(A, dim);
    stop = omp_get_wtime();
    check(A, dim);
    std::cout << "Dimension: " << dim;
    std::cout << ", threads: " << omp_get_max_threads();
    std::cout << ", execution time: " << stop - start << std::endl;
    delete[] A;
    return 0;
}