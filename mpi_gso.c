#include "mpi.h"
#include <iostream>
#include <cstdlib>
#include <cmath>
//#include "omp.h"

/* Margin of error. */
const double eps = 0.1;

/* Gram-Schmidt process. */
void gramSchmidt(double *A, int dim, int num)
{
    /* Get MPI info. */
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    double *buffer = new double[dim];
    double tmp;
    for (int i = 0; i < dim; ++i)
    {
        /* This process contains the current vector. */
        int proc = i / (dim / world_size);
        if (proc >= world_size)
        {
            proc = world_size - 1;
        }

        /* Local index of the current vector. */
        int local = i - proc * (dim / world_size);

        /* Normalize vector. */
        if (proc == world_rank)
        {
            tmp = 0.;
            //#pragma omp parallel for reduction(+:tmp)
            for (int j = 0; j < dim; ++j)
            {
                tmp += A[local * dim + j] * A[local * dim + j];
            }
            tmp = std::sqrt(tmp);
            //#pragma omp parallel for
            for (int j = 0; j < dim; ++j)
            {
                A[local * dim + j] /= tmp;
                buffer[j] = A[local * dim + j];
            }
        }

        /* Broadcast the current vector. */
        MPI_Bcast(buffer, dim, MPI_DOUBLE, proc, MPI_COMM_WORLD);

        /* Subtract projections. */
        if (proc > world_rank)
        {
            continue;
        }
        if (proc < world_rank)
        {
            local = -1;
        }
        //#pragma omp parallel for reduction(+:tmp)
        for (int j = local + 1; j < num; ++j)
        {
            tmp = 0.;
            for (int k = 0; k < dim; ++k)
            {
                tmp += buffer[k] * A[j * dim + k];
            }
            for (int k = 0; k < dim; ++k)
            {
                A[j * dim + k] -= tmp * buffer[k];
            }
        }
    }
    delete[] buffer;
}

void check(double *A, int dim, int num)
{
    /* Get MPI info. */
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    double *buffer = new double[dim];
    double tmp;
    for (int i = 0; i < dim; ++i)
    {
        /* This process contains the current vector. */
        int proc = i / (dim / world_size);
        if (proc >= world_size)
        {
            proc = world_size - 1;
        }

        /* Local index of the current vector. */
        int local = i - proc * (dim / world_size);

        /* Normalize vector. */
        if (proc == world_rank)
        {
            for (int j = 0; j < dim; ++j)
            {
                buffer[j] = A[local * dim + j];
            }
        }

        /* Broadcast the current vector. */
        MPI_Bcast(buffer, dim, MPI_DOUBLE, proc, MPI_COMM_WORLD);

        /* Check orthonormality. */
        for (int j = 0; j < num; ++j)
        {
            tmp = 0.;
            for (int k = 0; k < dim; ++k)
            {
                tmp += buffer[k] * A[j * dim + k];
            }
            if ((tmp > eps) && (std::abs(tmp - 1.) > eps))
            {
                std::cerr << tmp << std::endl;
            }
        }
    }
}

int main(int argc, char *argv[])
{
    /* Initialize MPI. */
    MPI_Init(&argc, &argv);
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    /* Initialize random and load parameter. */
    srand(time(NULL) + world_rank);
    int dim = atoi(argv[1]);
    if (dim < world_size)
    {
        std::cerr << "Too few vectors. Must be greater than number of MPI processes." << std::endl;
        MPI_Finalize();
        exit(1);
    }

    /* Generate set of random vectors. */
    int num = dim / world_size;
    if (world_rank == world_size - 1)
    {
        num += dim % world_size;
    }
    double *A = new double[num * dim];
    for (int i = 0; i < num; ++i)
    {
        for (int j = 0; j < dim; ++j)
        {
            A[i * dim + j] = rand() / (RAND_MAX + 1.);
        }
    }

    /* Measure performance. */
    double start, stop;
    MPI_Barrier(MPI_COMM_WORLD);
    start = MPI_Wtime();
    gramSchmidt(A, dim, num);
    MPI_Barrier(MPI_COMM_WORLD);
    stop = MPI_Wtime();
    //check(A, dim, num);
    if (world_rank == 0)
    {
        std::cout << "Dimension: " << dim;
        std::cout << ", processes: " << world_size;
        //std::cout << ", threads: " << omp_get_max_threads();
        std::cout << ", execution time: " << stop - start << std::endl;
    }
    delete[] A;
    MPI_Finalize();
    return 0;
}